---
title: "Über das Center"
description:
menu:
  main:
    weight: 1
---
# SDACC

The SDACC...

## Data Privacy

To protect your data, it is not enough to delete identifying features such as name or national insurance number. Important principles of "Statistical Disclosure Control" must also be observed. We are happy to help you create a completely anonymised data set that you can pass on to third parties without hesitation. Two of the most comprehensive R packages for statistical confidentiality - sdcMicro and sdcTable - were co-developed by us and we will be happy to help you adapt them to your circumstances.

