---
title: Contact
featured_image: ''
omit_header_text: true
description: Laissez-nous un message!
type: page
menu: main
---

Swiss Data Anonymization Compentence Center

Riggenbachstrasse 16, 4600 Olten

Höheweg 80, 2502 Biel

Emails:

murat.sariyar@bfh.ch

matthias.templ@fhnw.ch

https://sdacc.pages.fhnw.ch/
