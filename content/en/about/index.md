---
title: "About the Center"
description:
menu:
  main:
    weight: 1
---
# Swiss Data Anononymization Competence Center (SwissAnon)

We lead research on key topics in data anonymization and synthesis of data. 

We cater to researchers, businesses, and practitioners. 

The Anonymization Center supports you in secure data anonymization according to the requirements of the GDPR and Swiss national laws on data privacy. 

As the movement towards open data in the scientific community grows, data protection has emerged as a critical and inescapable concern for researchers and companies. This trend of easier data generation and dissemination is paralleled by stricter data protection measures, posing a challenge for researchers and companies: How can they ensure the privacy and security of their person-related data while also making their data openly available (internally or externally) to others?

We specialize in creating anonymized datasets through statistical techniques and generation of synthetic datasets that mirror the properties of actual data. This enables the secure sharing of information and the development and testing of AI models while ensuring privacy protection.

In addition, we develop free and open-source software for data anonymization and data synthetization and lead research in data anonymization and data synthesis. 


