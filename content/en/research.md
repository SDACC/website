---
title: Research
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
weight: 10

---

## Anonymization of data with personal-related information

Anonymizing data with personal-related information is vital for protecting individual privacy, complying with legal and ethical standards, maintaining public trust, and enabling the safe use of data for research and business purposes.
We facilitate both fundamental and advanced research, enhancing machine learning and artificial intelligence techniques to improve methods of data anonymization.

## Synthetic data as twin to original data

Synthetic data that serves as a twin to original data is crucial because it allows for the safe analysis and sharing of information without compromising the privacy and confidentiality of the actual data subjects. By mirroring the statistical properties of original datasets, synthetic data enables meaningful and accurate research, testing, and development, while mitigating the risks associated with using real, sensitive data. We are developing innovative methods that significantly surpass current tools by focusing on the unique structures in data, such as hierarchical relationships, familial connections like person-in-household, and logical associations, which are aspects typically not fully addressed (and to be able to completely learnt) by existing AI tools. Our approach specifically targets these complex data structures to provide more nuanced and accurate data analysis.

## Disclosre risk and data utility

Disclosure risk and data utility are crucial in data anonymization and synthesis because they balance the need to protect individual privacy with the usefulness of the data. High disclosure risk can lead to privacy breaches, violating ethical and legal standards, while low data utility can render the anonymized or synthesized data ineffective for meaningful analysis and decision-making. Therefore, achieving an optimal balance between minimizing disclosure risk and maximizing data utility is essential for maintaining privacy without compromising the data's value for research, analysis, or business intelligence.
However, existing disclosure risk measures are not designed for complex data. Moreover, new research is needed for better data utility measures. 

## Cross-sectional data

While there is a lot of research done for cross-section data, still the clever use of statistical models is ongoing research. 

## Longitudinal data

While there's been limited research on longitudinal data anonymization and synthesis, we recognize the immense value of such datasets for identifying trends. To address this gap, we've initiated a significant <a href="https://data.snf.ch/grants/grant/211751"> SNF Bridge Discovery project </a> focusing on the anonymization and synthesis of longitudinal data, which comes in various forms and is crucial for comprehensive trend analysis.

### Time series

Anonymization and synthetization of time series data involve advanced techniques to maintain the integrity and patterns of the original data over time, while ensuring that individual data points cannot be traced back to real individuals. This process is crucial for preserving privacy in datasets where temporal patterns and trends are key, allowing for meaningful analysis without compromising confidentiality.

### Mobility data

 This process is particularly vital in datasets tracking movements, such as those from GPS or mobile devices, to prevent the identification of individual trajectories while still allowing for the study of movement patterns and trends. Such data are high-dimensional by definition which causes research challenges. 

### Event history data

Anonymization and synthetization of event history data focus on altering or creating datasets that chronicle sequences of events or states (like employment history or medical records) in a way that preserves the overall patterns and sequences while safeguarding individual privacy. The challenges lies in changing information over time (e.g. educational or occupational status of persons). 

### Panel data 

Anonymization and synthetization of panel data involve modifying or generating data that tracks the same individuals or entities over multiple time periods, ensuring privacy while maintaining the longitudinal structure and relationships crucial for analysis. 


## Ongoing research projects

SNF Bride Discovery project on 
<a href="https://data.snf.ch/grants/grant/211751"> Harnessing event and longitudinal data in industry and health sector through privacy preserving technologies</a>



## Literature

<img src="/images/snf.PNG" alt="Statistical disclosure control for microdata"> 


Statistical disclosure control for microdata using the R-package sdcMicro
M Templ
Transactions on Data Privacy 1 (2), 67-85	120	2008

Statistical disclosure control for micro-data using the R package sdcMicro
M Templ, A Kowarik, B Meindl
Journal of Statistical Software 67, 1-36

Simulation of close-to-reality population data for household surveys with application to EU-SILC
A Alfons, S Kraft, M Templ, P Filzmoser
Statistical Methods & Applications 20 (3), 383-407	93	2011



Simulation of synthetic complex data: The R package simPop
M Templ, B Meindl, A Kowarik, O Dupriez
Journal of Statistical Software 79 (10), 1-38

Analysis of Commercial and Free and Open Source Solvers for the Cell Suppression Problem.
B Meindl, M Templ
Trans. Data Priv. 6 (2), 147-159

Disclosure risk of synthetic population data with application in the case of EU-SILC
M Templ, A Alfons
International Conference on Privacy in Statistical Databases, 174-186

Practical applications in statistical disclosure control using R
M Templ, B Meindl
Privacy and Anonymity in Information Management Systems: New Techniques for …

Simulation and quality of a synthetic close-to-reality employer–employee population
M Templ, P Filzmoser
Journal of Applied Statistics 41 (5), 1053-1072

sdcMicro: a new flexible R-package for the generation of anonymised microdata: Design issues and new methods
M Templ
Joint UNECE/Eurostat Work Session on Statistical Data Confidentiality …

sdcMicro: A package for statistical disclosure control in R
M Templ
Bulletin of the International Statistical Institute, 56th Session

New developments in statistical disclosure control and imputation: Robust statistics applied to official statistics
M Templ
Suedwestdeutscher Verlag fuer Hochschulschriften

The anonymisation of the CVTS2 and income tax dataset. an approach using R-package sdcmicro
B Meindl, M Templ
Joint UNECE/Eurostat Work Session on Statistical Data Confidentiality …	8	2007
Preservation of individuals’ privacy in shared COVID-19 related data

S Sauermann, C Kanjala, M Templ, CC Austin, RDA WG
Preservation of Individuals’ Privacy in Shared COVID-19 Related Data (July …

A graphical user interface for microdata protection which provides reproducibility and interactions: the sdcMicro GUI
M Templ, T Petelin
Trans. Data Priv. 2 (3), 207-224

Feedback-based integration of the whole process of data anonymization in a graphical interface
B Meindl, M Templ
Algorithms 12 (9), 191

Why shuffle when you can use robust statistics for SDC-a simulation study
M Templ, B Meindl
Privacy in Statistical Databases. Lecture Notes in Computer Science …

Privacy of study participants in open-access health and demographic surveillance system data: Requirements analysis for data anonymization
M Templ, C Kanjala, I Siems
JMIR Public Health and Surveillance 8 (9), e34472

Testing of IHSN C++ Code and Inclusion of New Methods into sdcMicro
A Kowarik, M Templ, B Meindl, F Fonteneau, B Prantner
Privacy in Statistical Databases: UNESCO Chair in Data Privacy …

A systematic overview on methods to protect sensitive data provided for various analyses
M Templ, M Sariyar
International Journal of Information Security 21 (6), 1233-1246