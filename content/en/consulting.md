---
title: Consulting
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
weight: 10

---

The Competence Center offers various services.

- Anonymization of data with person-related information
- Synthetic data as replacement for real data
- Consulting
- Research

<!-- The services of the Swiss Data Anonymization Competence Center are open to all academic disciplines and to research, public institutions and the private sector.-->

Our team has co-developed several extensive R packages for statistical confidentiality, synthetic data generation and record linkage

- sdcMicro (<a href="https://www.jstatsoft.org/article/view/v067i04"> Journal of Statistical Software Paper </a>, <a href="https://cran.r-project.org/web/packages/sdcMicro/index.html"> Software </a>)
- simPop (<a href="https://www.jstatsoft.org/article/view/v079i10"> Journal of Statistical Software Paper </a>, <a href="https://cran.r-project.org/web/packages/simPop/index.html"> Software</a>) 
- recordLinkage (Paper, Software)
- and we are working closely with other maintainers, e.g. R packages <a href="https://cran.r-project.org/web/packages/sdcTable/index.html"> sdcTable </a> and <a href="https://cran.r-project.org/web/packages/cellKey/index.html"> cellKey</a>.

and we are eager to customize them to fit your specific needs.