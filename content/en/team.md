---
title: Team
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
weight: 30
---

Core Team


<table>
  <tr>
    <td><img src="/website/images/team/placeholder.jpeg" alt="Murat Sariyar" style="width:100px;height:auto;"></td>
    <td><strong>Name:</strong> Murat Sariyar<br><strong>Description:</strong> Murat Sariyar's description here.</td>
  </tr>
  <tr>
    <td><img src="/website/images/team/placeholder.jpeg" alt="Matthias Templ" style="width:100px;height:auto;"></td>
    <td><strong>Name:</strong> Matthias Templ<br><strong>Description:</strong> Matthias Templ's description here.</td>
  </tr>
  <tr>
    <td><img src="/website/images/team/placeholder.jpeg" alt="Oscar Thees" style="width:100px;height:auto;"></td>
    <td><strong>Name:</strong> Oscar Thees<br><strong>Description:</strong> Oscar Thees's description here.</td>
  </tr>
  <tr>
    <td><img src="/website/images/team/placeholder.jpeg" alt="Marko Miletic" style="width:100px;height:auto;"></td>
    <td><strong>Name:</strong> Marko Miletic <br><strong>Description:</strong> Marko Miletic's description here.</td>
  </tr>
  <tr>
    <td><img src="/website/images/team/placeholder.jpeg" alt="Jiri Novak" style="width:100px;height:auto;"></td>
    <td><strong>Name:</strong> Jiri Novak <br><strong>Description:</strong> Jiri Novak's description here.</td>
  </tr>
</table>


Extended team and specialisms

<table>
  <tr>
    <td><img src="/website/images/team/placeholder.jpeg" alt="Carolin Strobl" style="width:100px;height:auto;"></td>
    <td><strong>Name:</strong> Carolin Strobl<br><strong>Description:</strong> Carolin Strobl's description here.</td>
  </tr>
</table>
