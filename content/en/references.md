---
title: References
featured_image: ''
omit_header_text: true
description: 
type: page
menu: main
weight: 20
---



<table >
  <tr>
    <td><img src="/website/images/references/Logo_BRIDGE_c.webp" alt="SNF Bridge" style="width:200px;height:auto;"></td>
    </td>
  </tr>
  <tr>
    <td><img src="/website/images/references/SNF_logo_standard_office_color_pos_e.png" alt="SNF" style="width:200px;height:auto;"></td>
  </tr>
</table>



HERE PICTURES OF SNF, SNF Bridge, world bank, oecd, paris21, IHSN, Inter-American Development Bank, Helsana,
Swisscom, SBB, EnerSuisse, MEIRU, Esther Switzerland, DIZH, Statistics Romania, Statistics Austria, Eurostat, 


### Our clients in Switzerland


- Anonymisation of data from **Helsana** Health Sciences 
  <a href="https://www.zhaw.ch/de/forschung/forschungsdatenbank/projektdetail/projektid/2453/" style="color:blue; text-decoration:underline;">LINK</a>
     * Anonymisation of the database on insured persons including pharmacy and hospital bills and stays  
- Evaluation of data anonymisation at Swisscom <a href="https://www.zhaw.ch/de/forschung/forschungsdatenbank/projektdetail/projektid/3671/" style="color:blue; text-decoration:underline;">LINK</a>
     * Anonymisation of the data lake and the anonymisation platform + Mobility data in applications/dashboards  
- Data anonymisation for SBB <a href="https://www.zhaw.ch/de/forschung/forschungsdatenbank/projektdetail/projektid/3715/" style="color:blue; text-decoration:underline;">LINK</a>
     * Courses and mobility data/trajectory data anonymisation   
- Smart-Meter data anonymization for EnerSuisse <a href="https://www.zhaw.ch/de/forschung/forschungsdatenbank/projektdetail/projektid/4117/" style="color:blue; text-decoration:underline;">LINK</a>
     * Suppressions for database queries  
- Better de-identification through statistical anonymisation for health data of the Malawian population <a href="https://www.esther-switzerland.ch/de/better-de-identification-using-statistical-anonymization-bisa-for-malawi-population-health-data/" style="color:blue; text-decoration:underline;">LINK</a>
     * Anonymisation of longitudinal health data
- Fellowship DIZH (Digital Initiative from  Kanton Zurich) \textit{Anonymisation and estimation of the re-identification risk of personal data}  


### Our clients and colloborators outside of Switzerland


- Anonymisation of Census data for Worldbank, grant XXX. (2022)
- Training Statistical Disclosure Control for Worldbank, grant XXX (2021)
- Anonymization of data for the Inter-American Development Bank (& €6k) (2021)
- Start-Up Grant from ESTHER Switzerland *Better de-Identification using Statistical Anonymisation (BISA) for Malawi population health data* (2020)
- World bank grant, selection number 7179283. *Re-designing the GUI for sdcMicro* (2016)
- Special grant agreement for statistical disclosure control, Eurostat. *"Tools for Statistical Disclosure Control"*   (2016)
- World bank grant, selection number 1206769. *Synthetic Data for Microsimulation*  (& €33k)  (2016)
- World Bank grant, selection number 1129231. *"Methods and tools for the generation of synthetic populations"*  (2015)
- Special grant agreement for statistical disclosure control, Eurostat. *"Generating Public-Use Files for EU-SILC and LFS"* (2012)
- Various of projects for the OECD and the International Household Survey Network on statistical disclosure control  (2008-2013)
- Subcontract for the European project *ESSnet on common
tools and harmonised methodology for SDC in the ESS*, Eurostat grant (& 
€20k) (2012)
- European project
*"Centre of Excellence in Statistical Disclosure Control"*, grant
agreement no 25200.2005.001-2005.619. Funding from the European
Commision/EUROSTAT (2006)


### Keynote speaches on data anonymization: 

- Juristinnentag (2022), Zurich
- Informatik, 51. Jahrestagung der Gesellschaft für Informatik (2021)
- Datenschutzrechtstagung (2021), Zurich
- Use of R in Official Statistics (uros 2020) in Vienna
International Conference on Computing, Mathematics and Statistics (iCMS2021) in Langkawi Island, Malaysia
- Symposium on Anonymisation of the Association for Data Protection Switzerland (2019)

### Tutorials and workshops

**Given Tutorials/Courses/Workshops**, e.g.  

*On-the-job training on Anonymization of Population data* for World Bank in 2022 in Bucharest; 
*Anonymization of Census data* for World Bank in 2021 (online);
*Anonymization of personal data with applications in R* at the Zurich R courses from the University of Zurich; 
*Data Anonymization Workshop* for the Inter-American Development Bank in 2021;  
*Population Health Data Anonymisation Workshop* at MEIRU in 2021; on 
*Anonymization* at SBB (2 workshops, 2020);
*Statistical Disclosure Control* at OECD in Paris 2013;

